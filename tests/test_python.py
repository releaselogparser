# -*- coding: utf-8 -*-
from rlogtester import ReleaseLogTestCase

class PythonReleseLogTestCase(ReleaseLogTestCase):
    type = 'python'
    kwargs = { } 
    input = """
v1.3, 2018-09-01 -- Don't throw exception on invalid tokens.
  Fix python 3 compatibility
v1.2, 2018-08-27 -- Fix handling of unrecognized closing tags.
v1.1, 2018-08-24 -- Initialize token_class dynamically.
v1.0, 2018-08-19 -- Initial release.
    
"""

    def test_count(self):
        self.assertEqual(len(self.rlog), 4, "wrong count")

    def test_recent(self):
        elt = self.rlog[0]
        self.assertEqual(elt.version, "1.3")

    def test_index(self):
        elt = self.rlog[2]
        self.assertEqual(elt.version, "1.1")

    def test_descr(self):
        self.assertEqual('\n'.join(self.rlog[0].descr),"""Don't throw exception on invalid tokens.
  Fix python 3 compatibility""")
        self.assertEqual('\n'.join(self.rlog[2].descr),"Initialize token_class dynamically.")

if __name__ == '__main__':
    unittest.main()
        
