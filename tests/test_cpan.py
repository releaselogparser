from rlogtester import ReleaseLogTestCase

class CPANReleseLogTestCase(ReleaseLogTestCase):
    type = 'CPAN'
    kwargs = { } 
    input = """
Revision history for Perl extension Config::HAProxy.

1.02  Mon Jul 16 07:38:31 2018
	- remove the leftover use of autodie

1.01  Thu Jul 12 09:04:28 2018
        - set minimal required Perl version
        - drop dependency on autodie

1.00  Sun Jul  8 19:57:57 2018
	- original revision

0.05  2018-05-20
        - experimental version
"""
    def test_count(self):
        self.assertEqual(len(self.rlog), 4, "wrong count")

    def test_recent(self):
        elt = self.rlog[0]
        self.assertEqual(elt.version, "1.02")

    def test_index(self):
        elt = self.rlog[2]
        self.assertEqual(elt.version, "1.00")

if __name__ == '__main__':
    unittest.main()
    
    
    
