from rlogtester import ReleaseLogTestCase

class GNUReleseLogTestCase(ReleaseLogTestCase):
    type = 'GNU'
    kwargs = { } 
    input = """
GNU mailutils NEWS -- history of user-visible changes. 2018-08-29
Copyright (C) 2002-2018 Free Software Foundation, Inc.
See the end of file for copying conditions.

Please send mailutils bug reports to <bug-mailutils@gnu.org>.

Version 3.5.90 (Git)

* Bugfixes

Version 3.5, 2018-08-29

* Support for Guile version 2.2.0 and later

Support for prior versions has been withdrawn.

* New scheme functions

** mu-encoder-port port name . args

Version 3.4 - 2017-11-02 

* Fix AM_MAILUTILS macro

The macro incorrectly compared three-part version number with a
two-part argument.

Version 3.3 - Sergey Poznyakoff, 2017-10-18

* TLS configuration

Some changes to the TLS configuration

Version 3.2 - 2017-03-11

* configuration syntax

Statements that allow for variable substitution also allow for command
expansion.  Commands are invoked the same way as in shell.    
"""
    
    def test_count(self):
        self.assertEqual(len(self.rlog), 4, "wrong count")

    def test_recent(self):
        elt = self.rlog[0]
        self.assertEqual(elt.version, "3.5")

    def test_index(self):
        elt = self.rlog[2]
        self.assertEqual(elt.version, "3.3")

if __name__ == '__main__':
    unittest.main()


